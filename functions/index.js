const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

const firestore = admin.firestore();

exports.triggerSecondScreenNotification = functions.firestore.document('second/{id}').onCreate(
    async (snapshot, context) => {

        const notificationPayload = {
            notification: {
                // received in on message
                title: "Second Screen",
                body: "For second screen",
                navigateTo: "second_screen",
            },
            data: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                navigateTo: "second_screen",
            }
        };

        var notificationResult = await admin.messaging().sendToTopic("test_notifications", notificationPayload);

    });

exports.triggerThirdScreenNotification = functions.firestore.document('third/{id}').onCreate(
    async (snapshot, context) => {

        const notificationPayload = {
            notification: {
                // received in on message
                title: "Third Screen",
                body: "For third screen",
                navigateTo: "third_screen",
            },
            data: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                navigateTo: "third_screen",
            }
        };

        var notificationResult = await admin.messaging().sendToTopic("test_notifications", notificationPayload);

    });
