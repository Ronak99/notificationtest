import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:notifications_test/service/push_notification_service.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      PushNotificationService().initialise(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FlatButton(
              child: Text("Second Screen"),
              onPressed: () async {
                print("tapped");
                Firestore.instance.collection("second").document().setData(
                    {"test": await FirebaseMessaging().getToken()}).then((_) {
                  print("data set");
                }).catchError((e) {
                  print(e);
                });
              },
            ),
            FlatButton(
              child: Text("Third Screen"),
              onPressed: () async {
                print("tapped");
                Firestore.instance.collection("third").document().setData(
                    {"test": await FirebaseMessaging().getToken()}).then((_) {
                  print("data set");
                }).catchError((e) {
                  print(e);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
