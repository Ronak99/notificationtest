import 'package:flutter/material.dart';
import 'package:notifications_test/second_screen.dart';
import 'package:notifications_test/third_screen.dart';

class NavigateNotification {
  void navigate(context, Map<String, dynamic> message) {
    var notificationData = message['data'];
    var navigateTo = notificationData['navigateTo'];

    if (navigateTo != null) {
      switch (navigateTo) {
        case "second_screen":
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SecondScreen(),
            ),
          );
          break;

        case "third_screen":
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ThirdScreen(),
            ),
          );
          break;

        default:
          print("default");
      }
    }
  }
}
