import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notifications_test/service/local_notification_service.dart';
import 'package:notifications_test/service/navigate_notification.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final LocalNotificationService localNotificationService =
      LocalNotificationService();
  final NavigateNotification _navigateNotification = NavigateNotification();

  Future initialise(BuildContext context) async {
    _fcm.subscribeToTopic("test_notifications");

    localNotificationService.init(context);

    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        localNotificationService.showLocalNotifications(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('onLaunch: $message');
        _navigateNotification.navigate(context, message);
      },
      onResume: (Map<String, dynamic> message) async {
        print('onResume: $message');
        _navigateNotification.navigate(context, message);
      },
    );
  }
}
