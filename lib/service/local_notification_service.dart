import 'dart:convert';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:notifications_test/service/navigate_notification.dart';

class LocalNotificationService {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  final NavigateNotification _navigateNotification = NavigateNotification();

  init(context) async {
    // initializing local notifications
    var android = AndroidInitializationSettings('mipmap/ic_launcher');
    var ios = IOSInitializationSettings();
    var platform = InitializationSettings(android, ios);
    bool isInitialized = await flutterLocalNotificationsPlugin.initialize(
      platform,
      onSelectNotification: (payload) =>
          onSelectLocalNotifications(context, payload),
    );
  }

  Future onSelectLocalNotifications(context, String payload) async {
    try {
      Map<String, dynamic> notificationData = jsonDecode(payload);
      _navigateNotification.navigate(context, notificationData);
    } catch (e) {
      print("error");
      print(e);
    }
  }

  Future<bool> showLocalNotifications(Map<String, dynamic> message) async {
    try {
      var notification = message['notification'];

      var android = AndroidNotificationDetails(
        'Test',
        'Test',
        "Test",
        priority: Priority.Max,
        playSound: true,
      );

      var iOS = IOSNotificationDetails(
        presentAlert: true,
        presentBadge: true,
        presentSound: true,
      );

      var platform = NotificationDetails(android, iOS);

      await flutterLocalNotificationsPlugin.show(
        0,
        notification['title'],
        notification['body'],
        platform,
        payload: json.encode(message),
      );
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
